import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javafx.embed.swing.SwingFXUtils;

import javafx.scene.image.WritableImage;
import org.opencv.core.*;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class BlurDetector {

    private List<String> listOfImagePaths = null;

    private BlurDetector(){
        //dummy
    }

    public BlurDetector(List<String> listOfImagePaths){
        this.listOfImagePaths = listOfImagePaths;
    }

    public LocatedWritableImage generateBlurryMap(double threshold, int squareROIsize, javafx.scene.image.Image image) {
        if(image instanceof LocatedWritableImage){
            return generateBlurryMap(threshold, squareROIsize, ((LocatedWritableImage)image).getURL());
        }else if(image instanceof LocatedImage){
            return generateBlurryMap(threshold, squareROIsize, ((LocatedImage)image).getURL());
        }
        return null;
    }

    public LocatedWritableImage generateBlurryMap(double threshold, int squareRoiSize, String imgPath) {
        Mat src = Imgcodecs.imread(imgPath);

        List<List<Mat>> segments = new ArrayList<>();

        int vCount =  src.height()/squareRoiSize;
        int hCount =  src.width()/squareRoiSize;

        ArrayList<ArrayList<Double>> laplacianValues = new ArrayList<>(vCount);
        for(int i=0; i<vCount; i++){
            laplacianValues.add(new ArrayList());
        }
        // Vertically
        for(int y = 0; y < vCount; y++){
            List<Mat> rowOfSegments = new ArrayList<>();
            // Horizontally
            for(int x = 0; x < hCount; x++){
                Rect rect = new Rect(x*squareRoiSize,y*squareRoiSize, squareRoiSize, squareRoiSize);
                Mat srcRoi = new Mat(src, rect);
                //Mat dstRoi = new Mat();
                ////////
                double calculatedBlur = calculateBlur(srcRoi);
                laplacianValues.get(y).add(calculatedBlur);
                Mat result = new Mat(srcRoi.rows(), srcRoi.cols(), srcRoi.type());

                for(int v=0; v<srcRoi.height(); v++){
                    for(int u=0; u<srcRoi.width(); u++){
                        double[] data = srcRoi.get(u, v);
                        int i = 2;
                        if (calculatedBlur < threshold){
                            i = 1;
                        }
                        data[0] = 0.0;
                        data[i] = 0.0;
                        result.put(u, v, data);
                    }
                }

                ////////
                rowOfSegments.add(result);
            }
            segments.add(rowOfSegments);
        }

        List<Mat> matRows = new ArrayList<>();
        for(List<Mat> rowOfSegments : segments){
            Mat joined = new Mat();
            Core.hconcat(rowOfSegments, joined);
            matRows.add(joined);
        }

        Mat matFinished = new Mat();
        Core.vconcat(matRows, matFinished);
        //*/

        Image img = HighGui.toBufferedImage(matFinished);
        LocatedWritableImage writableImage = new LocatedWritableImage(((BufferedImage) img).getWidth(), ((BufferedImage)img).getHeight());
        SwingFXUtils.toFXImage((BufferedImage) img, writableImage);
        writableImage.setUrl(imgPath);

        return writableImage;
    }


    private Mat makeLaplacianMask(Mat src, int squareRoiSize, double gammaValue){
        Mat result = new Mat(src.rows(), src.cols(), src.type());

        int vCount =  src.height()/squareRoiSize;
        int hCount =  src.width()/squareRoiSize;

        ArrayList<ArrayList<Double>> laplacianValues = new ArrayList<>(vCount);
        for(int i=0; i<vCount; i++){
            laplacianValues.add(new ArrayList());
        }

        double maxValue = 0.0;

        for(int y = 0; y < vCount; y++){
            for(int x = 0; x < hCount; x++){
                Rect rect = new Rect(x*squareRoiSize,y*squareRoiSize, squareRoiSize, squareRoiSize);
                Mat srcRoi = new Mat(src, rect);

                double calculatedBlur = calculateBlur(srcRoi);

                if(calculatedBlur > maxValue){
                    maxValue = calculatedBlur;
                }

                laplacianValues.get(y).add(calculatedBlur);
            }
        }

        double correctingFactor = 254.0 / maxValue;

        for(int y = 0; y < vCount; y++){
            for(int x = 0; x < hCount; x++){
                double valueToSet = laplacianValues.get(y).get(x) * correctingFactor;
                valueToSet = gammaCorrection(valueToSet, gammaValue, 0.0, 255.0);
                laplacianValues.get(y).set(x, valueToSet);
            }
        }

        for(int y = 0; y < vCount; y++){
            for(int x = 0; x < hCount; x++){
                double l = laplacianValues.get(y).get(x);

                for(int v=0; v<squareRoiSize; v++){
                    for(int u=0; u<squareRoiSize; u++){
                        double[] data = result.get(y*squareRoiSize+v, x*squareRoiSize+u);
                        if(data != null) {
                            data[0] = data[1] = data[2] = l;
                            result.put( y * squareRoiSize + v, x * squareRoiSize + u, data);
                        }
                    }
                }

            }
        }

        double gaussianSize = (squareRoiSize - (squareRoiSize % 2))*2.0 + 1.0;
        Imgproc.GaussianBlur(result, result, new Size(gaussianSize, gaussianSize), 0);

        return result;
    }

    public WritableImage processImage(int squareRoiSize, double gammaValue){
        Mat src1 = Imgcodecs.imread(listOfImagePaths.get(0));
        Mat src2 = Imgcodecs.imread(listOfImagePaths.get(1));
        Mat src1mask = makeLaplacianMask(src1, squareRoiSize, gammaValue);
        Mat src2mask = makeLaplacianMask(src2, squareRoiSize, gammaValue);

        Mat result = new Mat(src1mask.rows(), src1mask.cols(), src1mask.type());

        for(int y = 0; y < result.rows(); y++){
            for(int x = 0; x < result.cols(); x++){
                double[] mask1data = src1mask.get(y, x);
                double[] mask2data = src2mask.get(y, x);
                if(mask1data[0] > mask2data[0]){
                    result.put(y, x, src1.get(y, x));
                }else{
                    result.put(y, x, src2.get(y, x));
                }
            }
        }

        Image img = HighGui.toBufferedImage(result);
        WritableImage writableImage = new WritableImage(((BufferedImage) img).getWidth(), ((BufferedImage)img).getHeight());
        SwingFXUtils.toFXImage((BufferedImage) img, writableImage);

        return writableImage;
    }

    public LocatedWritableImage processImage2(int squareRoiSize, double gammaValue){
        Mat src1 = Imgcodecs.imread(listOfImagePaths.get(0));
        Mat src2 = Imgcodecs.imread(listOfImagePaths.get(1));

        Mat src2mask = makeLaplacianMask(src2, squareRoiSize, gammaValue);

        Mat result = new Mat(src1.rows(), src1.cols(), src1.type());

        for(int y = 0; y < result.rows(); y++){
            for(int x = 0; x < result.cols(); x++){
                double[] data = result.get(y, x);
                double[] mask2data = src2mask.get(y, x);
                double[] src1data = src1.get(y, x);
                double[] src2data = src2.get(y, x);

                double alpha = mask2data[0]/255.0;
                double beta = 1.0 - alpha;


                for(int i=0; i<3; i++){
                    data[i] = beta * src1data[i] + alpha * src2data[i];
                }

                result.put(y, x, data);
            }
        }

        Image img = HighGui.toBufferedImage(result);
        LocatedWritableImage writableImage = new LocatedWritableImage(((BufferedImage) img).getWidth(), ((BufferedImage)img).getHeight());
        SwingFXUtils.toFXImage((BufferedImage) img, writableImage);

        return writableImage;
    }

    public LocatedWritableImage generateMask(int squareROIsize, double gammaValue, javafx.scene.image.Image image) {
        if(image instanceof LocatedWritableImage){
            System.out.print("B");
            return generateMask(squareROIsize, gammaValue, ((LocatedWritableImage)image).getURL());
        }else if(image instanceof LocatedImage){
            System.out.print("C");
            return generateMask(squareROIsize, gammaValue, ((LocatedImage)image).getURL());
        }
        return null;
    }

    public LocatedWritableImage generateMask(int squareRoiSize, double gammaValue, String path){
        Mat src = Imgcodecs.imread(path);
        System.out.print("D");
        Mat srcMask = makeLaplacianMask(src, squareRoiSize, gammaValue);

        Image img = HighGui.toBufferedImage(srcMask);
        LocatedWritableImage writableImage = new LocatedWritableImage(((BufferedImage) img).getWidth(), ((BufferedImage)img).getHeight());
        SwingFXUtils.toFXImage((BufferedImage) img, writableImage);
        writableImage.setUrl(path);
        return writableImage;
    }

    public double calculateBlur(Mat matSample){
        Mat matGray = new Mat();
        Mat dst = new Mat();
        Imgproc.cvtColor(matSample, matGray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Laplacian(matGray, dst, CvType.CV_64F);
        MatOfDouble median = new MatOfDouble();
        MatOfDouble std = new MatOfDouble();
        Core.meanStdDev(dst, median, std);
        return Math.pow(std.get(0,0)[0],2);
    }

    private double gammaCorrection(double x, double correctionValue, double min, double max){
        double xx = x <= min ? min + 1.0 : x;
        xx = xx >= max ? max - 1.0 : xx;

        double j = correctionValue;
        return (xx*Math.pow(j-max,2.0))/(Math.pow(j, 2.0)+xx*(max-2*j));
    }
}
