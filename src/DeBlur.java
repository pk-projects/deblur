import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.opencv.core.Core;


public class DeBlur extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        System.out.println("App started...");
        //Loading the OpenCV core library
        System.loadLibrary( Core.NATIVE_LIBRARY_NAME );

        primaryStage.setTitle("DeBlur");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"));

        Parent root = loader.load();
        primaryStage.setScene(new Scene(root, 800, 650));

        MainWindowController mainWindowController = loader.getController();
        mainWindowController.setStage(primaryStage);

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}