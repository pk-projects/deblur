
public class DeBlurLauncher {

    public static void main(String[] args) {
        JarClassLoader jcl = new JarClassLoader();
        try {
            jcl.invokeMain("DeBlur", args);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}