import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;

public interface ISliderController <T>{
    Slider slider = null;
    TextField textField = null;
    CheckBox checkBox = null;
    double mainValue = 0.0;
    Boolean cutFraction = false;
    String hintText = "";

    double getMainValue();

    void initialize();

    void update();

    void setDisable(Boolean val);

    void setCutFraction(Boolean val);
}
