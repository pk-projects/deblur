import javafx.scene.image.Image;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class LocatedImage extends Image {
    private final String url;

    public LocatedImage(String url) throws FileNotFoundException {
        super(new FileInputStream(url));
        this.url = url;
    }

    public String getURL() {
        return url;
    }
}
