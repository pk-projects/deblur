import javafx.scene.image.WritableImage;


public class LocatedWritableImage extends WritableImage {
    private String url;

    public LocatedWritableImage(int width, int height) {
        super(width, height);
        url = "";
    }


    public String getURL() {
        return url;
    }
    public void setUrl(String newUrl){
        url = newUrl;
    }
}
