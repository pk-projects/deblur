import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


public class MainWindowController implements Initializable {
    private Stage stage = null;
    private double mainImageViewHeight = 0;
    private List<String> listOfImages;
    private BlurDetector blurDetector;
    private SliderController thresholdController;
    private SliderController fragmentationController;
    private SliderController gammaController;


    @FXML
    private ImageView mainImageView;
    @FXML
    private BorderPane mainImageBorderPane;
    @FXML
    private HBox thumbnailsPanel;
    @FXML
    private Slider thresholdSlider;
    @FXML
    private TextField thresholdTextField;
    @FXML
    private Slider fragmentationSlider;
    @FXML
    private TextField fragmentationTextField;
    @FXML
    private CheckBox previewCheck;
    @FXML
    private CheckBox maskCheck;
    @FXML
    private ImageView resultImageView;
    @FXML
    private TabPane tabPane;
    @FXML
    private TextField gammaTextField;
    @FXML
    private Slider gammaSlider;

    public MainWindowController(){
        listOfImages = new ArrayList<>();
        blurDetector = new BlurDetector(listOfImages);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        thresholdController = new SliderController(thresholdSlider, thresholdTextField, 100.0, "Threshold");
        thresholdController.setUpdateHandlerMethod(() -> updateBlurryRegions());

        fragmentationController= new SliderController(fragmentationSlider, fragmentationTextField, 20.0, "Fragmentation");
        fragmentationController.setUpdateHandlerMethod(() -> updateBlurryRegions());

        gammaController = new SliderController(gammaSlider, gammaTextField, 128.0, "Gamma");
        gammaController.setUpdateHandlerMethod(() -> updateBlurryRegions());

        fragmentationController.setCutFraction(true);

        mainImageBorderPane.getCenter().boundsInParentProperty().addListener(
                (observable, oldValue, newValue)-> {
                    mainImageViewHeight = newValue.getHeight();
                    if(mainImageViewHeight >= 10) {
                        mainImageView.setFitHeight(mainImageViewHeight - 1);
                    }
                });

        resultImageView.fitWidthProperty().bind(tabPane.widthProperty());

        previewCheck.selectedProperty().addListener((ov, old_val, new_val) -> {
            maskCheck.setDisable(new_val);
            if(new_val){
                updateBlurryRegions();
            }else{
                if(mainImageView.getImage() instanceof LocatedWritableImage){
                    try {
                        LocatedImage newImg = new LocatedImage(((LocatedWritableImage)mainImageView.getImage()).getURL());
                        mainImageView.setImage(newImg);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        maskCheck.selectedProperty().addListener((ov, old_val, new_val) -> {
            previewCheck.setDisable(new_val);
            if(new_val){
                updateBlurryRegions();
            }else{
                if(mainImageView.getImage() instanceof LocatedWritableImage){
                    try {
                        LocatedImage newImg = new LocatedImage(((LocatedWritableImage)mainImageView.getImage()).getURL());
                        mainImageView.setImage(newImg);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    public void setStage(Stage stage){
        this.stage = stage;
    }

    @FXML
    public void menuOpenFieldClicked(Event e) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image File(s)");
        List<File> fileList = fileChooser.showOpenMultipleDialog(stage);
        if(fileList != null) {
            for (File file : fileList) {
                if (file != null) {
                    try {
                        openImageAndAddToList(file.getAbsolutePath());
                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }

    @FXML
    public void menuSaveClicked(Event e) {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extensionFilter);


        fileChooser.setTitle("Save result");

        File file = fileChooser.showSaveDialog(stage);
        if(file != null){
            try {

                BufferedImage bufferedImage = SwingFXUtils.fromFXImage(resultImageView.getImage(), null);
                ImageIO.write(bufferedImage, "png", file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void openImageAndAddToList(String imagePath) throws FileNotFoundException {
        Image image = new LocatedImage(imagePath);
        listOfImages.add(((LocatedImage)image).getURL());

        ImageView imageView = new ImageView();

        imageView.setOnMouseClicked(event -> {
                    if(mainImageViewHeight > 10) {
                        mainImageView.setFitHeight(mainImageViewHeight - 1);
                    }else{
                        mainImageView.setFitHeight(10);
                    }
                    mainImageView.setImage(imageView.getImage());
                    updateBlurryRegions();
                }
        );

        imageView.setImage(image);
        imageView.setPreserveRatio(true);
        if(thumbnailsPanel.getPrefHeight()>10) {
            imageView.setFitHeight(thumbnailsPanel.getPrefHeight());
        }
        thumbnailsPanel.getChildren().add(imageView);

        mainImageView.setImage(image);
        mainImageView.fitWidthProperty().bind(mainImageBorderPane.widthProperty());
    }

    @FXML
    public void buttonProcessClicked(Event e) {
        Image image = blurDetector.processImage2((int)fragmentationController.getMainValue(), gammaController.getMainValue());
        resultImageView.setImage(image);

        tabPane.getSelectionModel().select(1);
    }

    private void updateBlurryRegions(){
        Image imageToSet = mainImageView.getImage();
        if(previewCheck.selectedProperty().get()) {
            imageToSet = blurDetector.generateBlurryMap(thresholdController.getMainValue(), (int) fragmentationController.getMainValue(), imageToSet);
        }else if(maskCheck.selectedProperty().get()){
            System.out.print("A");
            imageToSet = blurDetector.generateMask((int)fragmentationController.getMainValue(), gammaController.getMainValue(), imageToSet);
        }
        if(imageToSet != null) {
            mainImageView.setImage(imageToSet);
        }
    }

    //DEBUG METHOD
    @FXML
    public void imageInGalleryClicked(MouseEvent e) {
        e.consume();
        System.out.println("Image! " + e.getTarget());
        if(e.getTarget() instanceof ImageView){
            ImageView imageView0 = (ImageView)e.getTarget();
            String url = imageView0.getImage() instanceof LocatedImage ? ((LocatedImage) imageView0.getImage()).getURL() : null;
            System.out.println(url);
        }
    }

    public void updatePage(){
        //dummy
    }
}
