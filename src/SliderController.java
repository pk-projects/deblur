import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;

public class SliderController implements ISliderController{
    private Slider slider;
    private TextField textField;
    private double mainValue;
    private UpdateHandler updateHandler;
    private Boolean cutFraction = false;
    private String hintText;

    public SliderController(Slider slider, TextField textField, double defVal, String hintText){
        this.slider = slider;
        this.textField = textField;
        this.hintText = hintText;
        mainValue = defVal;
        initialize();
    }

    public double getMainValue(){
        return mainValue;
    }

    public void setUpdateHandlerMethod(UpdateHandler updateHandler){
        this.updateHandler = updateHandler;
    }

    public void initialize(){
        textField.setPromptText(hintText);
        if(textField.getParent() != null) {
            textField.getParent().requestFocus();
        }

        if(cutFraction){
            textField.textProperty().setValue(String.format("%.0f", String.valueOf(mainValue)));
        }else {
            textField.textProperty().setValue(String.valueOf(mainValue));
        }

        slider.valueProperty().addListener((ov, old_val, new_val) -> {
            mainValue = (double)new_val;
            if(cutFraction){
                textField.textProperty().setValue(String.format("%.0f", new_val));
            }else {
                textField.textProperty().setValue(new_val.toString());
            }
        });


        textField.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER){
                try{
                    double newValue = Double.parseDouble(textField.textProperty().getValue());
                    mainValue = newValue;
                    slider.valueProperty().setValue(newValue);
                    updateHandler.update();
                }catch (NumberFormatException e){
                    textField.textProperty().setValue(String.valueOf(mainValue));
                }
            }
        });

        slider.setOnMouseReleased(event -> updateHandler.update());
    }

    @Override
    public void update() {

    }

    @Override
    public void setDisable(Boolean val) {
        textField.setDisable(val);
        slider.setDisable(val);
    }

    @Override
    public void setCutFraction(Boolean val) {
        cutFraction = val;
    }
}
