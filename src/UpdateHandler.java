@FunctionalInterface
public interface UpdateHandler{
    void update();
}
